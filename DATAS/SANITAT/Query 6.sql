-- 6. Muestre a los enfermos nacidos a partir del año 1960

SELECT COGNOM, DATA_NAIX FROM sanitat.MALALT WHERE SUBSTRING(DATA_NAIX, 1, 4) > 1960;