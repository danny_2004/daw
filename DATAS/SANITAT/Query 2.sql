-- 2. Muestre los hospitales existentes (número, nombre y teléfono) que tengan una letra A en la segunda posición del nombre.

SELECT HOSPITAL_COD, NOM, TELEFON FROM sanitat.HOSPITAL WHERE SUBSTRING(NOM, 2, 1) = 'A' ;