# Consultas Sanitat Danny A. Calderon 21/02/2024 

* ## 1. Muestre los hospitales existentes (número, nombre y teléfono).

SELECT HOSPITAL_COD, NOM, TELEFON FROM sanitat.HOSPITAL;

* ## 2. Muestre los hospitales existentes (número, nombre y teléfono) que tengan una letra A en la segunda posición del nombre.

SELECT HOSPITAL_COD, NOM, TELEFON FROM sanitat.HOSPITAL WHERE SUBSTRING(NOM, 2, 1) = 'A' ;

* ## 3. Muestre los trabajadores (código hospital, código sala, número empleado y apellido) existentes.

SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM FROM sanitat.PLANTILLA;

* ## 4. Muestre los trabajadores (código hospital, código sala, número empleado y apellido) que no sean del turno de noche.

SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM FROM sanitat.PLANTILLA WHERE NOT TORN = 'N';

* ## 5. Muestre a los enfermos nacidos en 1960.

SELECT COGNOM, DATA_NAIX FROM sanitat.MALALT WHERE SUBSTRING(DATA_NAIX, 1, 4) = 1960;

* ## 6. Muestre a los enfermos nacidos a partir del año 1960

SELECT COGNOM, DATA_NAIX FROM sanitat.MALALT WHERE SUBSTRING(DATA_NAIX, 1, 4) > 1960;   