# Unidad C0: Recapitulación

Autor. Danny Alexandro Calderon

Introducción (sobre el documento o el tema tratado) -- mejor escribirla al final.

## Concepto y origen de las bases de datos
¿Qué son las bases de datos? ¿Qué problemas tratan de resolver? Definición de base de datos.

Una base de datos es un conjunto de datos estructurados que pertenecen a un mismo contexto y se utiliza para administrar de forma electrónica grandes cantidades de información.

* Más Almacenamiento y mas rapido
* Organizacion
* Seguridad
* Analisis de Datos

[Wikipedia](https://es.wikipedia.org/wiki/Base_de_datos)

## Sistemas de gestión de bases de datos
¿Qué es un sistema de gestión de bases de datos (DBMS)? ¿Qué características de acceso a los datos debería proporcionar? Definición de DBMS.

Es un software que permite la creación, administración y mantenimiento de una base de datos. El DBMS actúa como una interfaz entre la base de datos y los usuarios. Dan distintas herramientas como

* Almacenar Datos
* Modificar Datos
* Recuperar Datos
* Eliminar Datos
* Controlar Datos

[Wikipedia](https://es.wikipedia.org/wiki/Sistema_de_gestión_de_bases_de_datos)

### Ejemplos de sistemas de gestión de bases de datos
¿Qué DBMS se usan a día de hoy? ¿Cuáles de ellos son software libre? ¿Cuáles de ellos siguen el modelo cliente-servidor?

* Oracle DB : Ya no es tan popular, no es libre, utiliza cliente seridor
* IMB Db2 : Es muy utilizado, no es libre, utiliza cliente servidor
* SQLite : Es muy utilizado y facil de usar, es libre, no utiliza cliente servidor
* MariaDB : Es muy utilizado, es libre , utiliza cliente servidor
* SQL Server : Ya no es tan utilizado, no es libre, usa cliente servidor
* PostgreSQL : Es muy utiliado, es libre, es cliente servidor
* mySQL : Es muy utilizado, no es libre, usa cliente seridor

[Wikipedia](https://es.wikipedia.org/wiki/MySQL)
[Wikipedia](https://es.wikipedia.org/wiki/PostgreSQL)
[Wikipedia](https://es.wikipedia.org/wiki/MariaDB)
[Wikipedia](https://es.wikipedia.org/wiki/SQLite)
[Wikipedia](https://es.wikipedia.org/wiki/Oracle_Database)
[Wikipedia](https://es.wikipedia.org/wiki/Microsoft_SQL_Server)

## Modelo cliente-servidor
¿Por qué es interesante que el DBMS se encuentre en un servidor? ¿Qué ventajas tiene desacoplar al DBMS del cliente? ¿En qué se basa el modelo cliente-servidor?

* __Cliente__: Es el encargado de mandar peticiones a el servidor
* __Servidor__: Es el encargado de recibir las peticiones y ejecutarlas 
* __Red__: Es la encargada de conectar los clientes y servidores
* __Puerto de escucha__: los puertos permiten a los ordenadores deiferenciar facilmente los distintos tipos de trafico
* __Petición__: Cuando un proceso desea un servicio que proporciona cierto proceso, le envia un mensaje solitido ese servicio
* __Respuesta__: la resolucion de la peticion 

## SQL
¿Qué es SQL? ¿Qué tipo de lenguaje es?

El lenguaje de consulta estructurada (SQL) es un lenguaje de consulta popular que se usa con frecuencia en todos los tipos de aplicaciones. Los analistas y desarrolladores de datos aprenden y usan SQL porque se integra bien con los diferentes lenguajes de programación.

Es un lenguaje de consulta estructurado.

[amazon](https://aws.amazon.com/es/what-is/sql/#:~:text=es%20importante%20SQL%3F-,El%20lenguaje%20de%20consulta%20estructurada%20(SQL)%20es%20un%20lenguaje%20de,los%20diferentes%20lenguajes%20de%20programación.)

### Instrucciones de SQL

#### DDL : Define la estructura de la base de datos, incluyendo tablas, vistas e índices.
#### DML : Manipula los datos dentro de los objetos de la base de datos existentes.
#### DCL : Otorga y revoca permisos de acceso a los objetos de la base de datos para diferentes usuarios.
#### TCL : Administra cómo se confirman o se deshacen los cambios en los datos para garantizar la integridad de los mismos.

[W3Schools](https://www.w3schools.com/sql/)
[PostgreSQL](https://www.postgresql.org/docs/current/sql-commands.html)
[Microsoft SQL Server](https://docs.microsoft.com/en-us/sql/t-sql/statements/)

## Bases de datos relacionales
¿Qué es una base de datos relacional? ¿Qué ventajas tiene? ¿Qué elementos la conforman?

* __Relación (tabla)__: Trata de de una tabla que se une con varias entre si y se pueden enviar y recibir datos entre ella 
* __Atributo/Campo (columna)__:
* __Registro/Tupla (fila)__: Representa un registro

